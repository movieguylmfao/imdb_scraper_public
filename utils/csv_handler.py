import pandas as pd

class CSV_Handler():
    def __init__(self, csv_file):
        self.csv_file = csv_file
        
    def read_csv(self):
        df = pd.read_csv(self.csv_file)
        
        # Get Position, Title, Type, Year
        df = df[['Position', 'Const', 'Title', 'Title Type', 'Year', 'Created']]
        
        return df
    
    def to_json(self, df):
        return df.to_json('watchlist/watchlist.json', orient='records')