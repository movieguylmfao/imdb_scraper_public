import json
from datetime import datetime, timedelta

class Watchlist:
    def __init__(self, json_file):
        self.json_file = json_file
        self.watched_list = "./scraping/scraper/scraped_movies.json"
        
    def get_all(self):
        """Get all movies in the watchlist."""
        movies = []
        with open(self.json_file, "r") as file:
            data = json.load(file)
            for movie in data:
                movies.append(movie)
        return movies
        
    def add_json(self, title: str, json_file: str) -> None:
        """
        Adds a title to the JSON file.

        Args:
            title (str): The title to add to the JSON file.
            json_file (str): The path to the JSON file.
        """

        try:
            with open(json_file, 'r') as file:
                data = json.load(file)
        except (FileNotFoundError, json.JSONDecodeError):
            data = []

        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        data.append({"title": title, "date_added": now})

        with open(json_file, 'w') as file:
            json.dump(data, file, indent=4)
            
    def check_if_title_exists(self, title: str, json_file: str) -> bool:
        """
        Checks if a title exists in the JSON file.

        Args:
            title (str): The title to check for in the JSON file.
            json_file (str): The path to the JSON file.

        Returns:
            True if the title exists in the JSON file, False otherwise.
        """

        try:
            with open(json_file, 'r') as file:
                data = json.load(file)
        except (FileNotFoundError, json.JSONDecodeError):
            data = []

        for media in data:
            if 'title' in media and media['title'] == title:
                return True

        return False

    def check_duration(self, duration=24):
        """
        Check if movies in the watchlist were added more than `duration` hours ago.
        If a movie was added more than `duration` hours ago, remove it from the watchlist.

        Args:
            duration (int): The duration in hours.

        Returns:
            None.
        """
        with open(self.watched_list, "r") as file:
            data = file.read()
            if not data:
                print("File is empty.")
                return

            data = json.loads(data)

        expired_media = 0
        updated_data = []
        for movie in data:
            date_added = datetime.strptime(movie["date_added"], "%Y-%m-%d %H:%M:%S")
            if datetime.now() - date_added <= timedelta(hours=duration):
                updated_data.append(movie)
            else:
                expired_media += 1

        with open(self.watched_list, "w") as file:
            json.dump(updated_data, file)

        if expired_media > 0:
            print(f"{expired_media} expired media found, deleting.")
        else:
            print("No expired media found.")