import asyncio
import os
import subprocess
import sys
import time
import logging
import coloredlogs
from imdb.api.api import API
from scraping.providers.realdebrid import RealDebrid
from scraping.scraper.torrentio import Torrentio
from utils.csv_handler import CSV_Handler
from utils.watchlist import Watchlist
from profiles.profiles import Profiles
from profiles.profile_creator import ProfileCreator
import requests
import argparse
from dotenv import load_dotenv


class Main:
    def __init__(self):
        self.logger = self.setup_logger()
        self.check_version('version.txt', 'https://gitlab.com/movieguylmfao/imdb_scraper_public/-/raw/main/version.txt')
        self.profile = self.get_profile()
        self.scraper = Torrentio()
        self.rd = RealDebrid()
        self.api = API()
        self.csv_handler = CSV_Handler("watchlist/watchlist.csv")
        self.watchlist = Watchlist("watchlist/watchlist.json")
        self.watchlist_json = "./watchlist/watchlist.json"
        self.watched_list = "./scraping/scraper/scraped_movies.json"

    def setup_logger(self):
        logger = logging.getLogger(__name__)
        log_format = '[ %(levelname)s ] - [ %(asctime)s ] %(message)s'
        coloredlogs.install(level='INFO', logger=logger, fmt=log_format)
        return logger

    def check_version(self, local_file_path, online_file_url):
        with open(local_file_path, 'r') as file:
            local_file_content = file.read()

        response = requests.get(online_file_url)
        online_file_content = response.text

        if local_file_content == online_file_content:
            print("Currently using the latest version")
        else:
            print("There is an update available, do you want to update? (Y/N)")
            choice = input().lower()
            if choice == 'y':
                self.run_updater_in_new_terminal()
            else:
                print("Update cancelled. Please check the GitLab repo for more info.")
                print("Pausing for 10 seconds.")
                time.sleep(10)

    def run_updater_in_new_terminal(self):
        # Get the current working directory
        cwd = os.getcwd()

        # Construct the full path to auto_updater.py
        updater_path = os.path.join(cwd, 'auto_updater.py')

        if os.name == 'nt':  # For Windows
            os.system(f'start cmd /k python {updater_path}')
        elif sys.platform == 'darwin':  # For macOS
            os.system(f'''osascript -e 'tell app "Terminal" to do script "python3 {updater_path}"' ''')
        else:  # For Unix/Linux
            # Try different terminal emulators until one is found
            terminal_emulators = ['xterm', 'lxterminal', 'gnome-terminal', 'konsole', 'terminator', 'tilda']
            for terminal in terminal_emulators:
                try:
                    subprocess.Popen([terminal, '-e', f'python3 {updater_path}'])
                    break
                except FileNotFoundError:
                    continue
        sys.exit()

    def get_profile(self):
        args = self.parse_arguments()
        pc = Profiles()
        profile = pc.show_initial_menu() if not args.run else None

        if profile is None:
            pcc = ProfileCreator("profiles/profiles.json")
            profiles = pcc._load_data()
            profile = next((value for key, value in profiles["profiles"].items() if value["selected"]), None)

        # Reset the terminal, tested on macOS
        if os.name == 'nt':  # Windows
            os.system('cls')
        else:  # Unix/Linux/MacOS
            os.system('reset')

        return profile

    def parse_arguments(self):
        parser = argparse.ArgumentParser(description="IMDb Scraper")
        parser.add_argument("--run", action="store_true", help="Run the IMDb scraper directly without the initial menu.")
        return parser.parse_args()

    async def run_selenium(self):
        try:
            await asyncio.wait_for(self.api.get_watchlist(), timeout=10)
            return True
        except asyncio.TimeoutError:
            self.logger.error("Timeout error occurred while running Selenium")
            return False
        except Exception as e:
            self.logger.error(f"Error running Selenium: {e}")
            return False

    def run_csv_handler(self):
        self.csv_handler.to_json(self.csv_handler.read_csv())

    def check_watchlist(self):
        return self.watchlist.get_all()

    async def run(self):
        while True:
            await self.run_selenium()
            self.run_csv_handler()
            self.watchlist.check_duration(duration=168)
            watchlist = self.check_watchlist()

            for media in watchlist:
                if not self.watchlist.check_if_title_exists(media['Title'], self.watched_list):
                    if self.scraper.run(media['Const'], media['Title Type']):
                        self.logger.info(f"Successfully scraped {media['Title']}")
                        self.watchlist.add_json(media['Title'], self.watched_list)
                    else:
                        self.logger.error(f"Failed to scrape {media['Title']}. There is probably no torrent available.")
                        self.watchlist.add_json(media['Title'], self.watched_list)
            else:
                self.logger.info(f"No more items in watchlist.")
            await asyncio.sleep(5)


if __name__ == '__main__':
    load_dotenv()

    if os.environ.get("REALDEBRID_API_KEY") is None:
        api_key = input("Please enter your Real-Debrid API key (https://real-debrid.com/apitoken): ")
        with open(".env", "a") as env_file:
            env_file.write(f"REALDEBRID_API_KEY={api_key}\n")

    if os.environ.get("IMDB_WATCHLIST_URL") is None:
        print("Example: https://www.imdb.com/list/ls48923489/export (MAKE SURE IT CONTAINS EXPORT AT THE END)")
        imdb_watchlist_url = input("Please enter your IMDb watchlist URL: ")
        with open(".env", "a") as env_file:
            env_file.write(f"IMDB_WATCHLIST_URL={imdb_watchlist_url}\n")

    main = Main()
    asyncio.run(main.run())
