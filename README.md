# IMDB-Scraper: How to Run

Discord: https://discord.gg/UxTRx7mGAw

## Prerequisites
Tested on Python 3.9 or higher.

A IMDB Account

Make sure your IMDB watchlist is public: Go to your watchlist > Edit > Settings > Turn it to public

## Instructions

1. Clone the project repository or download all the files.
   ```shell
   git clone https://gitlab.com/movieguylmfao/imdb_scraper_public.git
   ```

   Or just download the zip from GitLab and extract it.

3. Run a terminal window in the folder imdb_scraper_public

2. Install the required dependencies using pip.
   ```shell
   pip install -r requirements.txt
   ```

3. Run the main script.
   ```shell
   python main.py
   ```

## Information
This project is designed to run on a device that is operational 24/7. It is specifically built to monitor your IMDB watchlist.

## How to get your IMDB Watchlist export link
Right click on Export this list and click "Copy link address"
![Alt text](imdb_image.PNG)

## Errors
If you run into an error or you want to clear your watchlist please run clean.py in the terminal
```shell
python clean.py
```
