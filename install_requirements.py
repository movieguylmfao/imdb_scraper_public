import subprocess

def install_requirements():
    requirements_file = 'requirements.txt'

    # Execute the pip install command
    subprocess.run(['pip', 'install', '-r', requirements_file], check=True)

    print("Requirements installed successfully.")

install_requirements()
