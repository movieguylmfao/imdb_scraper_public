import json
import os
from pathlib import Path

DEFAULT_PROFILES = {
    "profiles": {}
}

class ProfileCreator:
    def __init__(self, profile_path):
        self.profile_path = Path(profile_path)
        self.data = self._load_data()

    def _load_data(self):
        if self.profile_path.exists():
            with open(self.profile_path) as f:
                try:
                    data = json.load(f)
                    if data is None:
                        return DEFAULT_PROFILES
                    return data
                except json.JSONDecodeError:
                    return DEFAULT_PROFILES
        else:
            return DEFAULT_PROFILES

    def __getitem__(self, key):
        return self.data.get(key)

    def __setitem__(self, key, value):
        self.data[key] = value
        self._save_data()

    def _save_data(self):
        with open(self.profile_path, 'w') as f:
            json.dump(self.data, f)
