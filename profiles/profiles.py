import curses
import json
import os
import sys
import time
from pathlib import Path
from profiles.profile_creator import ProfileCreator
from dotenv import load_dotenv
from scraping.providers.torrentio import TorrentioManifestCreator

load_dotenv()

class Profiles:
    def __init__(self):
        self.profile_creator = ProfileCreator("profiles/profiles.json")
        self.profiles = self.profile_creator["profiles"]
        self.selected_profile = None
        
        if not self.profiles:
            self.create_default_profile()
            
    def cleanup(self):
        curses.nocbreak()
        self.stdscr.keypad(False)
        curses.echo()
        curses.endwin()
            
    def _get_selected_profile_name(self):
        for name, profile in self.profiles.items():
            if profile.get("selected", False):
                return name
        return None

    def _update_profiles(self):
        self.profile_creator = ProfileCreator("profiles/profiles.json")
        self.profile_creator["profiles"] = self.profiles
        self.profile_creator._save_data()
        self.selected_profile = self._get_selected_profile_name()
            
    def create_default_profile(self):
        default_profile = {
            "name": "Default",
            "scrape_amount": 1,
            "torrentio_manifest": 'providers=yts,eztv,rarbg,1337x,thepiratebay,kickasstorrents,torrentgalaxy,magnetdl,horriblesubs,nyaasi,rutor,rutracker,comando,bludv,torrent9,mejortorrent,cinecalidad|sort=quality',
            "selected": True
        }
        self.profiles["Default"] = default_profile
        self.profile_creator["profiles"] = self.profiles

    def show_initial_menu(self):
        # Initialize curses
        stdscr = curses.initscr()
        curses.cbreak()
        stdscr.keypad(True)

        # Define colors
        curses.start_color()
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        curses.init_pair(2, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)

        # Set up the initial menu
        initial_menu_options = [
            "Run Script",
            "Profile Configuration Menu",
            "Exit"
        ]
        current_option = 0

        while True:
            stdscr.clear()

            # Print the initial menu
            stdscr.addstr(2, 2, "Initial Menu", curses.color_pair(1))
            for i, option in enumerate(initial_menu_options):
                x = 4
                y = 4 + i * 2
                if i == current_option:
                    stdscr.addstr(y, x, option, curses.color_pair(3))
                else:
                    stdscr.addstr(y, x, option, curses.color_pair(2))

            # Load the profiles from the JSON file directly
            profiles = self.profile_creator._load_data()["profiles"]

            # Get the selected profile and its name
            selected_profile_name, selected_profile = \
            [(name, profile) for name, profile in profiles.items() if profile.get("selected", False)][0]

            # Check if the position is within the bounds of the window
            height, width = stdscr.getmaxyx()  # get the height and width of the window
            if 4 + len(initial_menu_options) * 2 < height and 2 < width:
                # Print the currently selected profile
                stdscr.addstr(4 + len(initial_menu_options) * 2, 2,
                              f"Currently selected profile: {selected_profile_name}", curses.A_BOLD)
            else:
                # Handle the error, e.g., by printing an error message or resizing the window
                print("Error: The position is outside the bounds of the window.")

            # Get user input
            key = stdscr.getch()

            # Handle user input
            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(initial_menu_options)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(initial_menu_options)
            elif key == ord("\n"):  # Enter key
                if current_option == 0:
                    # Clean up curses
                    curses.nocbreak()
                    stdscr.keypad(False)
                    curses.echo()
                    curses.endwin()

                    # Return the selected profile
                    return selected_profile
                elif current_option == 1:
                    # Profile Configuration Menu
                    go_back = self.show_profile_menu(stdscr)
                    if go_back == "go_back":
                        continue
                    else:
                        break
                elif current_option == 2:
                    sys.exit()

        # Clean up curses
        curses.nocbreak()
        stdscr.keypad(False)
        curses.echo()
        curses.endwin()

    def show_profile_menu(self, stdscr):
        # Set up the profile menu
        menu_options = [
            "Select Profile",
            "Create Profile",
            "View All Profiles",
            "Edit Profile",
            "Delete Profile",
            "Go Back"
        ]
        current_option = 0

        while True:
            stdscr.clear()

            # Print the profile menu
            stdscr.addstr(2, 2, "Profile Configuration Menu", curses.color_pair(1))
            for i, option in enumerate(menu_options):
                x = 4
                y = 4 + i * 2
                if i == current_option:
                    stdscr.addstr(y, x, option, curses.color_pair(3))
                else:
                    stdscr.addstr(y, x, option, curses.color_pair(2))

            stdscr.refresh()

            # Get user input
            key = stdscr.getch()

            # Handle user input
            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(menu_options)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(menu_options)
            elif key == ord("\n"):  # Enter key
                if current_option == 0:
                    self.select_profile(stdscr)
                    if self.selected_profile is not None:
                        self._update_profiles()
                        self.show_initial_menu()
                        break
                elif current_option == 1:
                    self.create_profile(stdscr)
                elif current_option == 2:
                    self.view_all_profiles(stdscr)
                elif current_option == 3:
                    self.edit_profile(stdscr)
                elif current_option == 4:
                    self.delete_profile(stdscr)
                elif current_option == 5:
                    return 'go_back'

        # Clean up curses
        curses.nocbreak()
        stdscr.keypad(False)
        curses.echo()
        curses.endwin()
        curses.endwin()
        
        self._update_profiles()


    def select_profile(self, stdscr):
        # Set up the profile selection menu
        profile_options = list(self.profiles.keys())
        current_option = 0

        while True:
            stdscr.clear()

            # Print the profile selection menu
            stdscr.addstr(2, 2, "Select Profile", curses.color_pair(1))
            stdscr.addstr(4, 4, "Select a profile:")

            for i, option in enumerate(profile_options):
                x = 6
                y = 6 + i * 2
                if i == current_option:
                    stdscr.addstr(y, x, option, curses.color_pair(3))
                else:
                    stdscr.addstr(y, x, option, curses.color_pair(2))

            stdscr.addstr(len(profile_options) * 2 + 8, 4, "Press 'q' to go back.")

            # Get user input
            key = stdscr.getch()

            # Handle user input
            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(profile_options)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(profile_options)
            elif key == ord("\n"):  # Enter key
                self.selected_profile = profile_options[current_option]
                for profile in self.profiles.values():
                    profile["selected"] = (profile["name"] == self.selected_profile)
                self.profile_creator["profiles"] = self.profiles
                self._update_profiles()
                break
            elif key == ord("q"):  # 'q' key
                self.selected_profile = None
                break



    def create_profile(self, stdscr):
        # Reload the data from the JSON file
        self.profiles = self.profile_creator._load_data()["profiles"]
        stdscr.clear()

        stdscr.addstr(2, 2, "Create Profile", curses.color_pair(1))

        stdscr.addstr(4, 4, "Enter the profile name:")
        stdscr.refresh()
        curses.echo()

        name = stdscr.getstr(6, 4, 30).decode("utf-8")

        if name:
            profile = {
                "name": name,
                "scrape_amount": "1",
                "torrentio_manifest": 'providers=yts,eztv,rarbg,1337x,thepiratebay,kickasstorrents,torrentgalaxy,magnetdl,horriblesubs,nyaasi,rutor,rutracker,comando,bludv,torrent9,mejortorrent,cinecalidad|sort=quality',
                "selected": False
            }
            self.profiles[name] = profile
            self.profile_creator["profiles"] = self.profiles

        curses.noecho()
        
        self._update_profiles()


    def view_all_profiles(self, stdscr):
        # Reload the data from the JSON file
        self.profiles = self.profile_creator._load_data()["profiles"]
        stdscr.clear()

        stdscr.addstr(2, 2, "View All Profiles", curses.color_pair(1))

        if self.profiles:
            row = 4
            for name, profile in self.profile_creator["profiles"].items():
                stdscr.addstr(row, 4, f"Name: {name}")
                row += 1
                for key, value in profile.items():
                    stdscr.addstr(row, 6, f"{key}: {value}")
                    row += 1
                stdscr.addstr(row, 4, "-" * 20)
                row += 1
        else:
            stdscr.addstr(4, 4, "No profiles found.")

        stdscr.addstr(row + 2, 4, "Press any key to continue.")
        stdscr.refresh()
        stdscr.getch()

    def edit_profile(self, stdscr):
        # Reload the data from the JSON file
        self.profiles = self.profile_creator._load_data()["profiles"]
        stdscr.clear()

        stdscr.addstr(2, 2, "Edit Profile", curses.color_pair(1))

        if self.profiles:
            profile_options = list(self.profiles.keys())
            current_option = 0

            while True:
                stdscr.clear()

                stdscr.addstr(4, 4, "Select a profile to edit:")

                for i, option in enumerate(profile_options):
                    x = 6
                    y = 6 + i * 2
                    if i == current_option:
                        stdscr.addstr(y, x, option, curses.color_pair(3))
                    else:
                        stdscr.addstr(y, x, option, curses.color_pair(2))

                stdscr.addstr(len(profile_options) * 2 + 8, 4, "Press 'q' to go back.")

                # Get user input
                key = stdscr.getch()

                # Handle user input
                if key == curses.KEY_UP:
                    current_option = (current_option - 1) % len(profile_options)
                elif key == curses.KEY_DOWN:
                    current_option = (current_option + 1) % len(profile_options)
                elif key == ord("\n"):  # Enter key
                    profile_name = profile_options[current_option]
                    self.edit_profile_details(stdscr, self.profiles[profile_name])
                    self.profile_creator["profiles"] = self.profiles
                elif key == ord("q"):  # 'q' key
                    break
        else:
            stdscr.addstr(4, 4, "No profiles found.")
            stdscr.addstr(6, 4, "Press any key to continue.")
            stdscr.refresh()
            stdscr.getch()
            
        self._update_profiles()

    def edit_profile_details(self, stdscr, profile, manifest=None):
        stdscr.clear()

        stdscr.addstr(2, 2, "Edit Profile Details", curses.color_pair(1))

        options = [
            ("Name", "name"),
            ("Scrape Amount", "scrape_amount"),
            ("Torrentio Manifest", "torrentio_manifest")
        ]
        current_option = 0

        while True:
            stdscr.clear()

            stdscr.addstr(4, 4, "Select a detail to edit:")

            for i, (option_text, option_key) in enumerate(options):
                x = 6
                y = 6 + i * 2
                if i == current_option:
                    stdscr.addstr(y, x, option_text, curses.color_pair(3))
                else:
                    stdscr.addstr(y, x, option_text, curses.color_pair(2))
                stdscr.addstr(y, x + 25, str(profile.get(option_key, "")))

            stdscr.addstr(len(options) * 2 + 8, 4, "Press 'q' to go back.")

            # Get user input
            key = stdscr.getch()

            # Handle user input
            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(options)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(options)
            elif key == ord("\n"):  # Enter key
                _, option_key = options[current_option]
                if option_key == "name":
                    continue  # Skip editing of name
                elif option_key == "torrentio_manifest":
                    stdscr.clear()
                    stdscr.addstr(2, 2, "Edit Profile Details", curses.color_pair(1))
                    stdscr.addstr(4, 4, "Executing config.run() for Torrentio Manifest...")
                    stdscr.refresh()

                    # Set the terminal to cbreak mode
                    curses.cbreak()

                    # Execute config.run() to get the manifest
                    config = TorrentioManifestCreator()
                    manifest = config.run(stdscr=stdscr, profile=profile)

                    # Reset the terminal to nocbreak mode
                    curses.nocbreak()

                    # Update the profile with the new manifest
                    profile[option_key] = manifest

                    # Show a success message
                    stdscr.clear()
                    stdscr.addstr(2, 2, "Edit Profile Details", curses.color_pair(1))
                    stdscr.addstr(4, 4, "The Torrentio Manifest has been updated. Please wait...")
                    stdscr.refresh()
                    time.sleep(2)  # Pause for 2 seconds

                    # Set the terminal to cbreak mode again
                    curses.cbreak()

                    # Enable the keypad again
                    stdscr.keypad(True)
                else:
                    stdscr.addstr(len(options) * 2 + 10, 4, "Enter new value:")
                    stdscr.refresh()
                    curses.echo()

                    new_value = stdscr.getstr(len(options) * 2 + 12, 4, 30).decode("utf-8")
                    profile[option_key] = new_value

                    curses.noecho()  # Reset input mode
            elif key == ord("q"):  # 'q' key
                curses.noecho()  # Reset input mode
                break

            # Refresh the screen
            stdscr.refresh()

            self._update_profiles()


    def delete_profile(self, stdscr):
        stdscr.clear()

        stdscr.addstr(2, 2, "Delete Profile", curses.color_pair(1))

        if self.profiles:
            profile_options = list(self.profiles.keys())
            current_option = 0

            while True:
                stdscr.clear()

                stdscr.addstr(4, 4, "Select a profile to delete:")

                for i, option in enumerate(profile_options):
                    x = 6
                    y = 6 + i * 2
                    if i == current_option:
                        stdscr.addstr(y, x, option, curses.color_pair(3))
                    else:
                        stdscr.addstr(y, x, option, curses.color_pair(2))

                stdscr.addstr(len(profile_options) * 2 + 8, 4, "Press 'q' to go back.")

                # Get user input
                key = stdscr.getch()

                # Handle user input
                if key == curses.KEY_UP:
                    current_option = (current_option - 1) % len(profile_options)
                elif key == curses.KEY_DOWN:
                    current_option = (current_option + 1) % len(profile_options)
                elif key == ord("\n"):  # Enter key
                    profile_name = profile_options[current_option]
                    if self.profiles[profile_name].get('selected', False):
                        stdscr.addstr(len(profile_options) * 2 + 10, 4, "Cannot delete the currently selected profile. Press any key to continue.")
                        stdscr.refresh()
                        stdscr.getch()
                    else:
                        del self.profiles[profile_name]
                        self.profile_creator["profiles"] = self.profiles
                        break
                elif key == ord("q"):  # 'q' key
                    break
        else:
            stdscr.addstr(4, 4, "No profiles found.")
            stdscr.addstr(6, 4, "Press any key to continue.")
            stdscr.refresh()
            stdscr.getch()

        self._update_profiles()

