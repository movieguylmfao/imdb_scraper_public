import os
from dotenv import load_dotenv
import requests
load_dotenv()

class TheMoviesDB:
    def __init__(self) -> None:
        if os.environ.get("MOVIESDB_API_KEY") is None:
            raise Exception("MOVIESDB_API_KEY environment variable not set.")
        self.api_token = os.environ.get("MOVIESDB_API_KEY")
        self.base_url = "https://api.themoviedb.org/3/"
        
    def get_serie_id(self, serie_name):
        response = requests.get(
            f"{self.base_url}search/tv",
            params={"query": serie_name, "include_adult": "false", "language": "en-US", "page": "1"},
            headers={"Authorization": f"Bearer {self.api_token}"},
        )
        
        return response.json()['results'][0]['id']
    
    def get_serie_details(self, serie_id):
        response = requests.get(
            f"{self.base_url}tv/{serie_id}",
            params={"language": "en-US"},
            headers={"Authorization": f"Bearer {self.api_token}"},
        )
        
        return response.json()