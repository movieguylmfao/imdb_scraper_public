import curses
import json
import requests
from bs4 import BeautifulSoup


class TorrentioManifestCreator:
    def __init__(self):
        self.base_url = "https://torrentio.strem.fun/configure"
        self.items_per_page = 9
        self.current_page = 1

    def get_all_values(self):
        response = requests.get(self.base_url)
        soup = BeautifulSoup(response.text, "html.parser")

        providers = self.inverse_select_options(soup, "#iProviders")
        sorting = self.select_options(soup, "#iSort", default_value="quality")
        languages = self.select_options(soup, "#iLanguages", remove_first_word=True)
        quality_filters = self.inverse_select_options(soup, "#iQualityFilter")

        return {
            "providers": providers,
            "sorting": sorting,
            "languages": languages,
            "quality_filters": quality_filters
        }

    def select_options(self, soup, selector, default_value=None, remove_first_word=False):
        options = []
        for option in soup.select(selector + " option"):
            value = option["value"]
            text = option.get_text(strip=True)
            if remove_first_word:
                text = ' '.join(text.split()[1:])  # Remove first word
            selected = value == default_value
            options.append({"value": value, "text": text, "selected": selected})
        return options

    def inverse_select_options(self, soup, selector):
        options = []
        for option in soup.select(selector + " option"):
            value = option["value"]
            text = option.get_text(strip=True)
            options.append({"value": value, "text": text, "selected": True})
        return options

    def construct_manifest(self, providers, sorting, languages, quality_filters):
        selected_providers = [provider["value"] for provider in providers if provider.get("selected", False)]
        selected_providers_str = ",".join(selected_providers)

        selected_sorting = [sort["value"] for sort in sorting if sort.get("selected", False)]

        if not selected_sorting:
            selected_sorting = ["quality"]

        selected_languages = [language["value"] for language in languages if language.get("selected", False)]
        selected_languages_str = ",".join(selected_languages) if selected_languages else ""

        selected_quality_filters = [quality_filter["value"] for quality_filter in quality_filters if not quality_filter.get("selected", True)]

        manifest_parts = []
        if selected_providers_str:
            manifest_parts.append(f"providers={selected_providers_str}")
        if selected_sorting:
            manifest_parts.append(f"sort={selected_sorting[0]}")
        if selected_languages_str:
            manifest_parts.append(f"language={selected_languages_str}")
        if selected_quality_filters:
            manifest_parts.append(f"qualityfilter={','.join(selected_quality_filters)}")

        manifest = "|".join(manifest_parts)
        return manifest

    def show_configuration_menu(self, stdscr):
        stdscr.clear()
        stdscr.keypad(True)

        stdscr.addstr(2, 2, "Configuration Menu", curses.A_BOLD)
        stdscr.refresh()

        menu_options = [
            "Providers",
            "Sorting",
            "Languages",
            "Quality Filters",
            "Done",
        ]
        current_option = 0

        while True:
            stdscr.clear()

            stdscr.addstr(2, 2, "Configuration Menu", curses.A_BOLD)
            for i, option in enumerate(menu_options):
                x = 4
                y = 4 + i * 2
                if i == current_option:
                    stdscr.addstr(y, x, option, curses.A_REVERSE)
                else:
                    stdscr.addstr(y, x, option)

            stdscr.refresh()

            key = stdscr.getch()

            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(menu_options)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(menu_options)
            elif key == ord("\n"):  # Enter key
                if current_option == 0:
                    self.select_providers(stdscr)
                elif current_option == 1:
                    self.select_sorting(stdscr)
                elif current_option == 2:
                    self.select_languages(stdscr)
                elif current_option == 3:
                    self.select_quality_filters(stdscr)
                elif current_option == 4:
                    return self.generate_manifest(stdscr)


        stdscr.keypad(False)

    def select_providers(self, stdscr):
        providers = self.config["providers"]
        page_size = 9  # Number of providers per page
        current_page = 1
        total_pages = (len(providers) + page_size - 1) // page_size
        current_option = 0

        while True:
            stdscr.clear()

            stdscr.addstr(2, 2, "Providers", curses.A_BOLD)
            stdscr.addstr(3, 2, "Select the providers you want to use", curses.A_DIM)
            stdscr.addstr(4, 2, "Use arrow keys to navigate, Enter to select/deselect, and 'q' to go back", curses.A_DIM)

            # Calculate the range of providers to display based on the current page
            start_index = (current_page - 1) * page_size
            end_index = min(start_index + page_size, len(providers))
            visible_providers = providers[start_index:end_index]

            for i, provider in enumerate(visible_providers):
                x = 4
                y = 6 + i
                selected = "✓" if provider.get("selected", False) else " "
                if i == current_option:
                    stdscr.addstr(y, x, f"{i + 1}. [{selected}] {provider['text']}", curses.A_REVERSE)
                else:
                    stdscr.addstr(y, x, f"{i + 1}. [{selected}] {provider['text']}")

            # Display the page information
            stdscr.addstr(stdscr.getmaxyx()[0] - 2, 2, f"Page: {current_page}/{total_pages}")

            stdscr.refresh()

            key = stdscr.getch()

            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(visible_providers)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(visible_providers)
            elif key == curses.KEY_LEFT:
                current_page = max(current_page - 1, 1)
                current_option = min(current_option, len(visible_providers) - 1)
            elif key == curses.KEY_RIGHT:
                current_page = min(current_page + 1, total_pages)
                current_option = min(current_option, len(visible_providers) - 1)
            elif key == ord("\n"):  # Enter key
                provider_index = start_index + current_option
                provider = providers[provider_index]
                provider["selected"] = not provider.get("selected", False)
            elif key == ord("q"):
                break

        self.current_page = current_page





    def select_sorting(self, stdscr):
        sorting = self.config["sorting"]
        current_option = 0

        while True:
            stdscr.clear()

            stdscr.addstr(2, 2, "Sorting", curses.A_BOLD)
            stdscr.addstr(3, 2, "Select the sorting you want to use", curses.A_DIM)
            stdscr.addstr(4, 2, "Use arrow keys to navigate, Enter to select/deselect, and 'q' to go back", curses.A_DIM)

            for i, sort in enumerate(sorting):
                x = 4
                y = 6 + i
                selected = "✓" if sort.get("selected", False) else " "
                if i == current_option:
                    stdscr.addstr(y, x, f"{i + 1}. [{selected}] {sort['text']}", curses.A_REVERSE)
                else:
                    stdscr.addstr(y, x, f"{i + 1}. [{selected}] {sort['text']}")

            stdscr.refresh()

            key = stdscr.getch()

            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(sorting)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(sorting)
            elif key == ord("\n"):  # Enter key
                # Unselect all other options
                for i, sort in enumerate(sorting):
                    sort["selected"] = False
                # Select the current option
                sort = sorting[current_option]
                sort["selected"] = True
            elif key == ord("q"):
                break


    def select_languages(self, stdscr):
        languages = self.config["languages"]
        page_size = 9  # Number of languages per page
        current_page = 1
        total_pages = (len(languages) + page_size - 1) // page_size
        current_option = 0

        while True:
            stdscr.clear()

            stdscr.addstr(2, 2, "Languages", curses.A_BOLD)
            stdscr.addstr(3, 2, "Select the foreign languages you want to prioritize", curses.A_DIM)
            stdscr.addstr(4, 2, "Use arrow keys to navigate, Enter to select/deselect, and 'q' to go back", curses.A_DIM)

            # Calculate the range of languages to display based on the current page
            start_index = (current_page - 1) * page_size
            end_index = min(start_index + page_size, len(languages))
            visible_languages = languages[start_index:end_index]

            for i, language in enumerate(visible_languages):
                x = 4
                y = 6 + i
                selected = "✓" if language.get("selected", False) else " "
                if i == current_option:
                    stdscr.addstr(y, x, str(f"{i + 1}. [{selected}] {language['text']}"), curses.A_REVERSE)
                else:
                    stdscr.addstr(y, x, str(f"{i + 1}. [{selected}] {language['text']}"))

            # Display the page information
            stdscr.addstr(stdscr.getmaxyx()[0] - 2, 2, f"Page: {current_page}/{total_pages}")

            stdscr.refresh()

            key = stdscr.getch()

            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(visible_languages)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(visible_languages)
            elif key == curses.KEY_LEFT:
                current_page = max(current_page - 1, 1)
                current_option = min(current_option, len(visible_languages) - 1)
            elif key == curses.KEY_RIGHT:
                current_page = min(current_page + 1, total_pages)
                current_option = min(current_option, len(visible_languages) - 1)
            elif key == ord("\n"):  # Enter key
                language_index = start_index + current_option
                language = languages[language_index]
                language["selected"] = not language.get("selected", False)
            elif key == ord("q"):
                break


    def select_quality_filters(self, stdscr):
        quality_filters = self.config["quality_filters"]
        current_option = 0

        while True:
            stdscr.clear()

            stdscr.addstr(2, 2, "Quality Filters", curses.A_BOLD)
            stdscr.addstr(3, 2, "Select the quality filter you want to use", curses.A_DIM)
            stdscr.addstr(4, 2, "Use arrow keys to navigate, Enter to select/deselect, and 'q' to go back", curses.A_DIM)

            for i, quality_filter in enumerate(quality_filters):
                x = 4
                y = 6 + i
                selected = "✓" if quality_filter.get("selected", True) else " "
                if i == current_option:
                    stdscr.addstr(y, x, f"{i + 1}. [{selected}] {quality_filter['text']}", curses.A_REVERSE)
                else:
                    stdscr.addstr(y, x, f"{i + 1}. [{selected}] {quality_filter['text']}")

            stdscr.refresh()

            key = stdscr.getch()

            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(quality_filters)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(quality_filters)
            elif key == ord("\n"):  # Enter key
                quality_filter = quality_filters[current_option]
                quality_filter["selected"] = not quality_filter.get("selected", True)
            elif key == ord("q"):
                break

        self.config["quality_filters"] = quality_filters


    def generate_manifest(self, stdscr):
        manifest = self.construct_manifest(**self.config)

        stdscr.clear()

        if manifest:
            return manifest
        else:
            stdscr.addstr(2, 2, "No providers selected. Manifest is empty.", curses.A_BOLD)

        stdscr.refresh()
        stdscr.getch()


    def cleanup(self, stdscr):
        # Clean up curses
        curses.nocbreak()
        stdscr.keypad(False)
        curses.echo()
        curses.endwin()
        
    def run(self, stdscr=None, profile=None):
        if not stdscr:
            raise ValueError("stdscr must be provided")

        # Add the title
        stdscr.addstr(0, 2, "Manifest Generator", curses.color_pair(2))

        # Add the menu options
        menu_options = [
            "1. Select Providers",
            "2. Select Sorting",
            "3. Select Languages",
            "4. Select Quality Filters",
            "5. Save and exit",
        ]

        # Create the configuration menu object and get the initial values
        self.config = self.get_all_values()

        current_option = 0
        manifest = None

        # Wait for user input
        while True:
            stdscr.clear()

            # Add the menu options
            stdscr.addstr(2, 2, "Configuration Menu", curses.A_BOLD)
            for i, option in enumerate(menu_options):
                x = 4
                y = 4 + i * 2
                if i == current_option:
                    stdscr.addstr(y, x, option, curses.A_REVERSE)
                else:
                    stdscr.addstr(y, x, option)

            stdscr.refresh()

            key = stdscr.getch()

            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(menu_options)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(menu_options)
            elif key == ord("\n"):  # Enter key
                if current_option == 0:
                    self.select_providers(stdscr)
                elif current_option == 1:
                    self.select_sorting(stdscr)
                elif current_option == 2:
                    self.select_languages(stdscr)
                elif current_option == 3:
                    self.select_quality_filters(stdscr)
                elif current_option == 4:
                    self.cleanup(stdscr)
                    return self.generate_manifest(stdscr)


    def old_run(self):
        # Initialize curses
        stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        stdscr.keypad(True)

        # Set the cursor to be invisible
        curses.curs_set(0)

        # Set the color scheme
        curses.start_color()
        curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_BLUE)
        curses.init_pair(2, curses.COLOR_BLUE, curses.COLOR_WHITE)
        curses.init_pair(3, curses.COLOR_WHITE, curses.COLOR_BLACK)

        # Set the color scheme
        stdscr.bkgd(curses.color_pair(1))

        # Add the title
        stdscr.addstr(0, 2, "Plex Manifest Generator", curses.color_pair(2))

        # Add the menu options
        menu_options = [
            "1. Select Providers",
            "2. Select Sorting",
            "3. Select Languages",
            "4. Select Quality Filters",
            "5. Generate Manifest",
            "6. Exit"
        ]

        # Create the configuration menu object and get the initial values
        self.config = self.get_all_values()

        current_option = 0

        # Wait for user input
        while True:
            stdscr.clear()

            # Add the menu options
            stdscr.addstr(2, 2, "Configuration Menu", curses.A_BOLD)
            for i, option in enumerate(menu_options):
                x = 4
                y = 4 + i * 2
                if i == current_option:
                    stdscr.addstr(y, x, option, curses.A_REVERSE)
                else:
                    stdscr.addstr(y, x, option)

            stdscr.refresh()

            key = stdscr.getch()

            if key == curses.KEY_UP:
                current_option = (current_option - 1) % len(menu_options)
            elif key == curses.KEY_DOWN:
                current_option = (current_option + 1) % len(menu_options)
            elif key == ord("\n"):  # Enter key
                if current_option == 0:
                    self.select_providers(stdscr)
                elif current_option == 1:
                    self.select_sorting(stdscr)
                elif current_option == 2:
                    self.select_languages(stdscr)
                elif current_option == 3:
                    self.select_quality_filters(stdscr)
                elif current_option == 4:
                    self.generate_manifest(stdscr)
                elif current_option == 5:
                    break

        self.cleanup(stdscr)



        

