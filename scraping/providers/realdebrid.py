import requests
import os
from dotenv import load_dotenv
load_dotenv()

class RealDebrid:
    def __init__(self):
        
        if os.environ.get("REALDEBRID_API_KEY") is None:
            raise Exception("REALDEBRID_API_KEY environment variable not set.")
        self.api_token = os.environ.get("REALDEBRID_API_KEY")
        self.base_url = "https://api.real-debrid.com/rest/1.0/"
        self.torrent_id = None
        self.files_id = None

    def add_magnet(self, magnet_link):
        """
        Add the magnet link to RealDebrid. 
        :param magnet_link: The magnet link to add.
        :return: None.
        """
        
        response = requests.post(
            f"{self.base_url}torrents/addMagnet",
            data={"magnet": magnet_link},
            headers={"Authorization": f"Bearer {self.api_token}"},
        )

        response_json = response.json()
        print(response_json)
        if "id" in response_json:
            self.torrent_id = response_json["id"]
            #print(f'SELF.TORRENT_ID: {self.torrent_id}')
            self.select_files()
        else:
            print("Failed to add magnet.")

    def select_files(self):
        """
        Add all files to the download.
        :return: None.
        """
        response = requests.post(
            f"{self.base_url}torrents/selectFiles/{self.torrent_id}",
            data={"files": "all"},
            headers={"Authorization": f"Bearer {self.api_token}"},
        )

        if response.status_code == 204:
            print("Successfully added.")
            return True
        
        return False
    
    def instant_availabilty(self, torrent_hashes):
        url = f"{self.base_url}torrents/instantAvailability"
        for torrent_hash in torrent_hashes:
            url += f"/{torrent_hash}"

        response = requests.get(
            url,
            headers={"Authorization": f"Bearer {self.api_token}"},
        )

        if response.status_code == 200:
            file_ids = response.json()
            # Process the list of file IDs as needed
            print(file_ids)
        else:
            print("Failed to get instant availability.")
        



if __name__ == "__main__":
    rd = RealDebrid()