import json
import logging
import os
import re
import time
import requests
from dotenv import load_dotenv
from urllib.parse import quote
from lxml import html
import coloredlogs

from profiles.profile_creator import ProfileCreator
from scraping.scraper.scraper import Scraper

load_dotenv()

logger = logging.getLogger(__name__)

# Define the log format
log_format = '[ %(levelname)s ] - [ %(asctime)s ] %(message)s'

# Install the coloredlogs package
coloredlogs.install(level='INFO', logger=logger, fmt=log_format)

class Torrentio(Scraper):
    """
    Torrentio scraper class. Extends the Scraper base class.
    """
    def __init__(self):
        """
        Initializes the Torrentio scraper.
        """
        super().__init__('https://torrentio.strem.fun/')
        self.base_url = "https://torrentio.strem.fun/"

        pcc = ProfileCreator("profiles/profiles.json")
        profiles = pcc._load_data()

        # Find the one where selected is true
        for key, value in profiles["profiles"].items():
            if value["selected"] == True:
                self.profile = value
                break
            
        #logger.info(f"Using profile: {self.profile}")

        self.manifest = self.prepare_manifest()
        self.scrape_amount = self.profile["scrape_amount"]

        self.added_torrents = set()  # Added set to track added torrents

        #logger.info(f"Torrentio manifest: {self.manifest}")
        
    def scrape_episode(self, imdb_id):
        url = f"https://www.imdb.com/title/{imdb_id}/"
        headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
        }   
        response = requests.get(url, headers=headers)
        
        tree = html.fromstring(response.content)

        # Extract season and episode
        se_info = tree.xpath('/html/body/div[2]/main/div/section[1]/section/div[3]/section/section/div[2]/div/div[1]/div')[0]
        season_episode = re.search(r'S(\d+).E(\d+)', se_info.text_content())
        season, episode = season_episode.groups()

        logger.info(f"Season: {season}, Episode: {episode}")

        # Extract series IMDB ID
        series_link = tree.xpath('/html/body/div[2]/main/div/section[1]/section/div[3]/section/section/div[1]/a/@href')[0]
        series_imdb_id = re.search(r'/title/(tt\d+)/', series_link).group(1)

        logger.info(f"Series IMDB ID: {series_imdb_id}")

        return series_imdb_id, season, episode

    def prepare_manifest(self) -> str:
        """
        Prepares the manifest for usage.

        Returns:
            str: The prepared manifest.
        """
        manifest = self.profile["torrentio_manifest"]
        scrape_amount = self.profile["scrape_amount"]
        
        # Add limit to the manifest
        manifest += f"|limit={scrape_amount}"
        
        #logger.warning(f"Prepared manifest: {manifest}")
        
        return manifest



    def scrape(self, imdb_id: str, type: str, s: str = None, e: str = None) -> dict:
        """
        Scrapes the given IMDB ID.

        Args:
            imdb_id (str): The IMDB ID to scrape.
            type (str): The type of the media.
            s (str, optional): Season number for series. Defaults to None.

        Returns:
            dict: The scraped data.
        """
        logger.info(f"Scraping: IMDB ID: {imdb_id}, Type: {type}, Season: {s}, Episode: {e}")
        
        #logger.warning(f"MANISFEST: {self.manifest}")
        
        if type == "movie":
            url = f"{self.base_url}{self.manifest}/stream/movie/{imdb_id}.json"
        elif type == "tvSeries":
            url = f"{self.base_url}{self.manifest}/stream/series/{imdb_id}:{s}:{e}.json"
        elif type == "tvEpisode":
            series_imdb_id, season, episode = self.scrape_episode(imdb_id)
            
            logger.info(f"Scraping episode: {series_imdb_id}, {season}, {episode}")
            
            url = f"{self.base_url}{self.manifest}/stream/series/{series_imdb_id}:{season}:{episode}.json"
        else:
            logger.error(f"Invalid type: {type}")
            return json.loads("{}")

        logger.info(f"Scraping URL: {url}")
        return self.fetch(url)


    def fetch(self, url: str) -> dict:
        """
        Fetches data from the given URL.

        Args:
            url (str): The URL to fetch data from.

        Returns:
            dict: The fetched data.
        """
        try:
            r = requests.get(url)
            return r.json()
        except Exception as e:
            logger.error(f"Failed to fetch data: {e}")
            return {}

    def process_json(self, json_data: dict) -> list:
            """
            Processes the given JSON data.

            Args:
                json_data (dict): The JSON data to process.

            Returns:
                list: The processed data.
            """
            streams = json_data.get('streams', [])
            results = [
                {
                    'Title': stream.get('title').strip(),
                    'Info Hash': stream.get('infoHash').strip(),
                    'Seeders': stream.get('title')[stream.get('title').find('👤') + 2 : stream.get('title').find('💾') - 1].strip() if '👤' in stream.get('title') else None,
                    'Size': stream.get('title')[stream.get('title').find('💾') + 2 : stream.get('title').find('⚙️') - 1].strip() if '💾' in stream.get('title') else None,
                    'Provider': stream.get('title')[stream.get('title').find('⚙️') + 2 : ].strip() if '⚙️' in stream.get('title') else None
                }
                for stream in streams
            ]

            return results

    def build_magnet(self, info_hash: str, title: str) -> str:
        """
        Builds a magnet link from the given info hash and title.

        Args:
            info_hash (str): The info hash.
            title (str): The title.

        Returns:
            str: The magnet link.
        """
        try:
            return f"magnet:?xt=urn:btih:{info_hash}&dn={quote(title)}"
        except Exception as e:
            logger.error(f"Failed to build magnet link: {e}")
            return None

    def add_to_rd(self, magnet_link: str):
        """
        Adds the given magnet link to Real Debrid.

        Args:
            magnet_link (str): The magnet link to add.
        """
        super().upload_to_realdebrid(magnet_link)

    def check_episode(self, title: str):
        """Checks if the title corresponds to a single episode."""
        # Only match single episodes, not full seasons or complete series
        return not self.check_season(title) and not self.check_multi_season(title) and not self.check_complete_season(title) and 'E' in title



    def check_season(self, title: str):
        """Checks if the title corresponds to a full season."""
        match_season = re.search(r'S(\d+)', title, re.IGNORECASE)
        match_episode = re.search(r'E(\d+)', title, re.IGNORECASE)
        return match_season is not None and match_episode is None and not self.check_complete_season(title)


    def check_complete_season(self, title: str):
        """Checks if the title corresponds to a complete season."""
        match_complete = re.search(r'complete', title, re.IGNORECASE)
        return match_complete is not None

    def check_multiple_seasons(self, title: str):
        """Checks if the title corresponds to multiple seasons."""
        match = re.search(r'S(\d+)-S(\d+)|Season (\d+)-(\d+)', title, re.IGNORECASE)
        return match is not None

    def get_last_season(title: str):
        """Gets the last season from a title that corresponds to multiple seasons."""
        match = re.search(r'S(\d+)-S(\d+)', title, re.IGNORECASE)
        if match:
            return int(match.group(2))  # group(2) corresponds to the second season number in the match
        return None
    
    def check_multi_season(self, title: str) -> int:
        """Checks if the title corresponds to multiple seasons and returns the last season number."""
        # This regular expression looks for patterns like 'Seasons 01-19' or 'Season 01-19'
        match = re.search(r'Seasons? (\d+)-(\d+)', title, re.IGNORECASE)
        if match:
            return int(match.group(2))  # group(2) corresponds to the second season number in the match
        return None

    def run(self, imdb_id: str, type: str) -> bool:
        if type == "tvSeries":
            return self.process_tv_series(imdb_id)
        else:
            return self.process_movie(imdb_id)

    def process_tv_series(self, imdb_id: str) -> bool:
        season = 1
        while True:
            episode = 1
            while True:
                results = self.process_json(self.scrape(imdb_id, "tvSeries", season, episode))

                # If no results for the first episode of a new season, assume no more seasons
                if not results and episode == 1:
                    logger.warning(f"No more seasons for {imdb_id}.")
                    return True

                # If no results for a non-first episode, move to next season
                elif not results:
                    logger.warning(f"Season {season} fully downloaded or no episodes found. Moving to the next season.")
                    break

                # Process the first result
                self.process_result(results[0])
                episode += 1

            season += 1

    def process_movie(self, imdb_id: str) -> bool:
        results = self.process_json(self.scrape(imdb_id, "movie"))

        if not results:
            logger.error(f"Could not find magnet for imdb_id '{imdb_id}' OR season has ended")
            return False

        for result in results:
            self.process_result(result)

        return True

    def process_result(self, result):
        magnet_link = self.build_magnet(result['Info Hash'], result['Title'])
        if result['Info Hash'] not in self.added_torrents:
            self.add_to_rd(magnet_link)
            self.added_torrents.add(result['Info Hash'])
            logger.info(f"Scraped title: {result['Title']}")
        else:
            logger.info(f"Torrent already added. Skipping: {result['Title']}")




