import sys
import asyncio
import time
import aiohttp
from bs4 import BeautifulSoup
from scraping.scraper.scraper import Scraper


class X1337(Scraper):
    def __init__(self):
        super().__init__('https://www.1337xx.to/search/')

    def query_builder(self, query):
        query = super().query_builder(query)
        print(query)
        return query
    
    async def search(self, query, type, max_pages=3):
        """
        type is either 'movie' or 'tvSeries'
        """
        print(f"Searching for {query} as {type}...")

        original_query = query  # Store the original query string
        titles = []  # Create an empty list to store titles

        async with aiohttp.ClientSession() as session:
            query = query.replace(' ', '%20')
            page = 1
            tasks = []
            while page <= max_pages:
                url = f"https://www.1337xx.to/search/{query}/{page}/"
                print(url)
                tasks.append(asyncio.create_task(self.fetch(session, url, titles, original_query)))
                page += 1
            
            await asyncio.gather(*tasks)

        # Sort the titles by resolution and seeders
        titles.sort(key=lambda x: (self.resolution_order(x["resolution"]), -x["seeders"]))
        
        # Sort by highest size
        #titles = self.sort_by_size(titles)
        
        # Print amount of torrents found
        print(f"Found {len(titles)} torrents.")

        # return the best match
        return titles[0]

    async def fetch(self, session, url, titles, original_query):
        """
        Fetches data from the specified URL using an asynchronous session.

        Args:
            session (aiohttp.ClientSession): The asynchronous session to use for making requests.
            url (str): The URL to fetch data from.
            titles (list): A list to store the fetched titles.
            original_query (str): The original query used for filtering titles.

        Returns:
            None
        """

        # Fetch HTML content from the URL using the session
        async with session.get(url) as response:
            response.raise_for_status()
            html = await response.text()

        # Parse the HTML using BeautifulSoup
        soup = BeautifulSoup(html, 'html.parser')

        # Select the table rows containing the desired elements
        tr_elements = soup.select("body > main > div > div > div > div.box-info-detail.inner-table > div > table > tbody > tr")

        # Check if any torrents were found
        if not tr_elements:
            print("No torrents found")
            return

        # Loop through each table row
        for tr_element in tr_elements:
            # Extract the title element and its text
            title_element = tr_element.select_one("td.coll-1.name > a:nth-child(2)")
            title = title_element.text.strip()

            # Extract the href link for the title
            href_link = f"https://www.1337xx.to{title_element['href']}"

            # Extract the seeder count and convert it to an integer
            seeder_element = tr_element.select_one("td.coll-2.seeds")
            seeder = int(seeder_element.text.strip())

            # Extract the leecher count and convert it to an integer
            leecher_element = tr_element.select_one("td.coll-3.leeches")
            leecher = int(leecher_element.text.strip())

            # Extract the size of the torrent
            size_element = tr_element.select_one("td.coll-4.size.mob-uploader")
            size = size_element.text.strip()

            # Fetch the magnet link for the title
            magnet = await self.get_magnet(session, href_link)

            # Extract the resolution from the title
            resolution = self.extract_resolution(title)
            
            # Extract the hash
            hash = await self.get_hash(session, href_link)

            # Append the title and its related information to the titles list
            titles.append({
                "title": title,
                "seeders": seeder,
                "leechers": leecher,
                "size": size,
                "href_link": href_link,
                "magnet": magnet,
                "resolution": resolution,
                "hash": hash,
            })


    def extract_resolution(self, title):
        resolutions = ["1080", "720", "480", "2160"]
        for resolution in resolutions:
            if resolution in title:
                return resolution
        return "UNKNOWN"
            
    def resolution_order(self, resolution):
        return ["1080", "720", "480", "2160", "UNKNOWN"].index(resolution)


    async def get_magnet(self, session, href_link):
        async with session.get(href_link) as response:
            response.raise_for_status()
            html = await response.text()

        soup = BeautifulSoup(html, 'html.parser')
        magnet_element = soup.select_one("body > main > div > div > div > div.l01666766a6e72ac1e0b35a029d916ebeb4c2ea24.no-top-radius > div.l30719a994ed675b3e5543484a83d6141b0edb709.clearfix > ul.l9007bbd0b313f4aa20553ab76822d4971c77b323.ldcb0d226eccf9ef57b49d77ef2a5c194f84fb666 > li.dropdown > ul > li:nth-child(4) > a")
        magnet = magnet_element['href'] if magnet_element else ""

        return magnet
        
    async def get_hash(self, session, href_link):
        """
        value of body > main > div > div > div > div.l01666766a6e72ac1e0b35a029d916ebeb4c2ea24.no-top-radius > div.manage-box > div > p:nth-child(2) > span
        """
        async with session.get(href_link) as response:
            response.raise_for_status()
            html = await response.text()
        
        soup = BeautifulSoup(html, 'html.parser')
        hash_element = soup.select_one("body > main > div > div > div > div.l01666766a6e72ac1e0b35a029d916ebeb4c2ea24.no-top-radius > div.manage-box > div > p:nth-child(2) > span")
        hash = hash_element.text.strip() if hash_element else ""
        
        return hash
        
        
    def sort_by_size(self, titles):
        return super().sort_by_size(titles)
        
    async def upload_to_realdebrid(self, magnet_link):
        return super().upload_to_realdebrid(magnet_link)