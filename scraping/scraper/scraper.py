import sys
from scraping.providers.realdebrid import RealDebrid
import re


class Scraper():
    def __init__(self, website):
        self.website = website

    def query_builder(self, query):
        query = query.replace(' ', '%20')
        return query
    
    def upload_to_realdebrid(self, magnet_link):
        """
        Uploads a magnet link to RealDebrid.

        :param magnet_link: A string representing the magnet link to be uploaded.
        :return: None
        """
        rd = RealDebrid()
        rd.add_magnet(magnet_link)
        
    def sort_by_size(self, titles):
        """
        Sorts the titles by size.

        :param titles: A list of titles.
        :return: A list of titles sorted by size.
        """
        # Convert size values to bytes
        for title in titles:
            size = title["size"]
            gb_pos = size.find("GB")
            mb_pos = size.find("MB")
            if gb_pos != -1:
                size = float(size[:gb_pos]) * 1024 * 1024 * 1024
            elif mb_pos != -1:
                size = float(size[:mb_pos]) * 1024 * 1024
            else:
                size = float(size)
            title["size_bytes"] = size

        # Sort titles by size
        sorted_titles = sorted(titles, key=lambda x: x["size_bytes"], reverse=True)

        # Remove the temporary "size_bytes" field
        for title in sorted_titles:
            del title["size_bytes"]

        return sorted_titles


if __name__ == "__main__":
    scraper = Scraper('https://www.1337xx.to/search/')
    print(scraper.query_builder('python'))