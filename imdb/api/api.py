import aiohttp
import asyncio
from bs4 import BeautifulSoup
import os
from dotenv import load_dotenv

load_dotenv()

class API():
    def __init__(self):
        if os.environ.get("IMDB_WATCHLIST_URL") is None:
            raise Exception("IMDB_WATCHLIST_URL environment variable not set.")
        self.watchlist_url = os.environ.get("IMDB_WATCHLIST_URL")

    async def get_watchlist(self, delay=15):
        while True:
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get(self.watchlist_url) as response:
                        if response.status != 200:
                            print(f"Error {response.status}: Unexpected status code. Retrying in {delay} seconds.")
                            await asyncio.sleep(delay)
                            continue
                        os.makedirs('watchlist', exist_ok=True)
                        with open('watchlist/watchlist.csv', 'wb') as file:
                            while True:
                                chunk = await asyncio.wait_for(response.content.read(1024), timeout=10)
                                if not chunk:
                                    break
                                file.write(chunk)
                        await session.close()
                        return True
            except asyncio.TimeoutError:
                print("Timeout error occurred while downloading watchlist")
                return False
            except Exception as e:
                print(f"Error downloading watchlist: {e}")
                return False
