import os


def clean_files():
    csv_file = 'watchlist/watchlist.csv'
    json_file = 'watchlist/watchlist.json'
    scraped_movies_file = 'scraping/scraper/scraped_movies.json'

    # Clear watchlist CSV file
    if os.path.exists(csv_file):
        with open(csv_file, 'w'):
            pass

    # Clear watchlist JSON file
    if os.path.exists(json_file):
        with open(json_file, 'w'):
            pass

    # Clear scraped movies JSON file
    if os.path.exists(scraped_movies_file):
        with open(scraped_movies_file, 'w'):
            pass

    print("Files cleared successfully.")

clean_files()
