import os
import shutil
import subprocess
import time
import urllib.request
from zipfile import ZipFile
import logging
import sys


class AutoUpdater:
    def __init__(self):
        self.logger = logging.getLogger(__name__)
        self.url = 'https://gitlab.com/movieguylmfao/imdb_scraper_public/-/archive/main/imdb_scraper_public-main.zip'
        self.file_name = 'update.zip'
        self.ignore_files = [
            'scraping/scraper/scraped_movies.json',
            'profiles/profiles.json',
            '.env',
            'watchlist/watchlist.json',
            'watchlist/watchlist.csv',
            'auto_updater.py',
        ]

    def check_permissions(self):
        """Check if the updater has the necessary permissions to modify the files."""
        self.logger.info("Checking permissions...")

        # Get the directory where the script is located
        script_dir = os.path.dirname(os.path.abspath(__file__))

        # Walk through script directory and subdirectories
        for root, dirs, files in os.walk(script_dir):
            # Remove hidden directories and files in-place
            dirs[:] = [d for d in dirs if not d.startswith('.')]
            files = [f for f in files if not f.startswith('.')]

            # Check each file
            for file in files:
                file_path = os.path.join(root, file)
                if not os.access(file_path, os.W_OK | os.R_OK):
                    self.logger.error("No read/write permission for file: %s", file_path)
                    return False

            # Check each directory
            for dir in dirs:
                dir_path = os.path.join(root, dir)
                if not os.access(dir_path, os.W_OK | os.R_OK):
                    self.logger.error("No read/write permission for directory: %s", dir_path)
                    return False

        self.logger.info("Permissions check passed.")
        return True

    def download_zip(self):
        self.logger.info("Downloading update from %s", self.url)
        urllib.request.urlretrieve(self.url, self.file_name)
        self.logger.info("Download complete.")

    def update_files(self):
        print("Updating files...")
        with ZipFile(self.file_name, 'r') as zip_ref:
            # Get the top-level directory within the zip file
            top_level_dir = zip_ref.namelist()[0]

            # Remove any trailing slash from the top_level_dir
            top_level_dir = top_level_dir.rstrip("/")

            print(f"Top-level directory: {top_level_dir}")

            # Extract all files and directories within the top-level directory
            for item in zip_ref.namelist():
                # Skip the top-level directory itself
                if item.startswith(top_level_dir):
                    item_path = item[len(top_level_dir):].lstrip("/")
                    if item_path in self.ignore_files:
                        continue

                    # Use os.path.join to construct item_path relative to the script's current directory
                    item_path = os.path.join(os.path.dirname(__file__), item_path)

                    print(f"Extracting {item} to {item_path}")

                    target_dir = os.path.dirname(item_path)
                    print(f"Target directory: {target_dir}")

                    if os.path.exists(item_path):
                        if os.path.isdir(item_path):
                            # Skip directories if they already exist
                            continue

                    if item.endswith('/'):  # If the item is a directory
                        os.makedirs(item_path, exist_ok=True)
                    else:  # If the item is a regular file
                        os.makedirs(target_dir, exist_ok=True)
                        with zip_ref.open(item) as source, open(item_path, "wb") as target:
                            shutil.copyfileobj(source, target)

        os.remove(self.file_name)

        print("\n" * 10)
        print("Update complete. Please restart main.py from your old terminal window.")
        print("This window will close in 15 seconds.")

        time.sleep(15)

        os.system("exit")

    def update(self):
        if not self.check_permissions():
            self.logger.error("Updater does not have the necessary permissions to modify the files.")
            return
        self.download_zip()
        self.update_files()
        # self.restart_program()

    def restart_program(self):
        """Restarts the main.py script"""
        self.logger.info("Restarting main.py...")
        python = sys.executable
        script_dir = os.path.dirname(os.path.abspath(__file__))
        main_script = os.path.join(script_dir, "main.py")
        subprocess.Popen([python, main_script])
        sys.exit()


if __name__ == '__main__':
    updater = AutoUpdater()
    updater.update()
